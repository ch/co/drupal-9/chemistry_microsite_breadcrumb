<?php

namespace Drupal\chemistry_microsite_breadcrumb\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * Microsite breadcrumb block.
 *
 * @Block(
 *   id = "microsite_breadcrumb",
 *   admin_label = "Microsite breadcrumb and login link"
 * )
 */
class MicrositeBreadcrumbBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Config factory.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * The file URL generator service.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, ThemeHandlerInterface $theme_handler, FileUrlGeneratorInterface $file_url_generator, AccountInterface $current_user, RouteProviderInterface $route_provider) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->themeHandler = $theme_handler;
    $this->fileUrlGenerator = $file_url_generator;
    $this->currentUser = $current_user;
    $this->routeProvider = $route_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
    $configuration,
    $plugin_id,
    $plugin_definition,
    $container->get('config.factory'),
    $container->get('theme_handler'),
    $container->get('file_url_generator'),
    $container->get('current_user'),
    $container->get('router.route_provider'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->configFactory->get('chemistry_microsite_breadcrumb.settings');
    $breadcrumb_title = $config->get('breadcrumb_title');
    $breadcrumb_url = $config->get('breadcrumb_url');

    $content = [];

    if ($breadcrumb_title && $breadcrumb_url) {

      $theme_path = $this->themeHandler->getTheme('pl9')->getPath();
      $home_image_path = $theme_path . '/images/interface/icon-breadcrumb-home.png';
      $home_image_url = $this->fileUrlGenerator->generateAbsoluteString($home_image_path);

      $content['breadcrumb_image'] = [
        '#theme' => 'image',
        '#uri' => $home_image_url,
        '#alt' => $breadcrumb_title,
      ];

      $content['breadcrumb_link'] = [
        '#type' => 'link',
        '#title' => $breadcrumb_title,
        '#url' => Url::fromUri($breadcrumb_url),
        '#attributes' => [
          'class' => ['microsite-breadcrumb'],
        ],
      ];

    }

    if ($this->currentUser->isAnonymous()) {
      try {
        $this->routeProvider->getRouteByName('ucam_entra_authn.login');

        $login_url = Url::fromRoute('ucam_entra_authn.login');
        $content['login_link'] = [
          '#type' => 'link',
          '#title' => $this->t('Login'),
          '#url' => $login_url,
          '#attributes' => [
            'class' => ['ucam-login-link'],
          ],
        ];
      }
      catch (RouteNotFoundException $e) {
        // Do nothing if the ucam_entra_authn.login route does not exist.
      }
    }

    return [
      'content' => $content,
      '#attached' => [
        'library' => ['chemistry_microsite_breadcrumb/chemistry_microsite_breadcrumb'],
      ],
    ];
  }

}
